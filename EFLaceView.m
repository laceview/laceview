//
//  EFLaceView.m
// EFLaceView
//
//  Created by MacBook Pro ef on 01/08/06.
//  Copyright 2006 Edouard FISCHER. All rights reserved.
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//	-	Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//	-	Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//	-	Neither the name of Edouard FISCHER nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#import "EFLaceView.h"
#import "EFView.h"

static void *_propertyObservationContext = (void *)1091;
static void *_dataObjectsObservationContext = (void *)1092;
static void *_selectionIndexesObservationContext = (void *)1093;


#warning TOTO : implement delegates



@implementation EFLaceView

#pragma mark -
#pragma mark *** utility functions***

float treshold(float x,float tr)
{
	return (x>0)?((x>tr)?x:tr):-x+tr;
}

#pragma mark -
#pragma mark *** init routines ***

- (id) initWithFrame:(NSRect)frame {
	self = [super initWithFrame:frame];
	if (self != nil) {
		
	}
	return self;
}

- (void) dealloc {
	
	[super dealloc];
}


#pragma mark -
#pragma mark *** bindings ***

+ (void)initialize
{
	[self exposeBinding:@"dataObjects"];
	[self exposeBinding:@"selectionIndexes"];
	[self setKeys:[NSArray arrayWithObjects:@"dataObjects",nil] triggerChangeNotificationsForDependentKey:@"laces"];
}

- (NSArray *)exposedBindings
{
	return [NSArray arrayWithObjects:@"dataObjects", @"selectedObjects", nil];
} 

- (void)bind:(NSString *)bindingName
	toObject:(id)observableObject
 withKeyPath:(NSString *)observableKeyPath
	 options:(NSDictionary *)options
{
    if ([bindingName isEqualToString:@"dataObjects"])
	{
		[self setDataObjectsContainer:observableObject];
		[self setDataObjectsKeyPath:observableKeyPath];
		[dataObjectsContainer addObserver:self
								forKeyPath:dataObjectsKeyPath
								   options:(NSKeyValueObservingOptionNew |
											NSKeyValueObservingOptionOld)
								   context:_dataObjectsObservationContext];
		[self startObservingDataObjects:[dataObjectsContainer valueForKeyPath:dataObjectsKeyPath]];
		
		
    }
	else if ([bindingName isEqualToString:@"selectionIndexes"])
	{
		[self setSelectionIndexesContainer:observableObject];
		[self setSelectionIndexesKeyPath:observableKeyPath];
		[selectionIndexesContainer addObserver:self
									 forKeyPath:selectionIndexesKeyPath
										options:nil
										context:_selectionIndexesObservationContext];
    }
	[super bind:bindingName
	   toObject:observableObject
	withKeyPath:observableKeyPath
		options:options];
	
    [self setNeedsDisplay:YES];
}


- (void)unbind:(NSString *)bindingName {
	
    if ([bindingName isEqualToString:@"dataObjects"])
	{
		[self stopObservingDataObjects:[dataObjectsContainer valueForKeyPath:dataObjectsKeyPath]];
		[dataObjectsContainer removeObserver:self forKeyPath:dataObjectsKeyPath];
		[self setDataObjectsContainer:nil];
		[self setDataObjectsKeyPath:nil];
    }
	
	if ([bindingName isEqualToString:@"selectionIndexes"])
	{
		[selectionIndexesContainer removeObserver:self forKeyPath:selectionIndexesKeyPath];
		[self setSelectionIndexesContainer:nil];
		[self setSelectionIndexesKeyPath:nil];
	}
	[super unbind:bindingName];
	[self setNeedsDisplay:YES];
}

- (void)startObservingDataObjects:(NSArray *)dataObjects
{
	if ([dataObjects isEqual:[NSNull null]])
	{
		return;
	}
	/*
	 Register to observe each of the new dataObjects, and
	 each of their observable properties -- we need old and new
	 values for drawingBounds to figure out what our dirty rect
	 */
	NSEnumerator *dataObjectsEnumerator = [dataObjects objectEnumerator];
	
	id newDataObject;
	
    while ((newDataObject = [dataObjectsEnumerator nextObject]))
	{
		[newDataObject addObserver:self
						forKeyPath:@"drawingBounds"
						   options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
						   context:_propertyObservationContext];
		EFView *dummy = [[EFView alloc] init];
		[self addSubview:dummy];
		
		[self scrollRectToVisible:[dummy bounds]]; //make new view visible if view in scrolling view
		
		// bind view to data
		[dummy bind:@"title" toObject:newDataObject withKeyPath:@"title" options:nil];
		[dummy bind:@"titleColor" toObject:newDataObject withKeyPath:@"titleColor" options:nil];
		[dummy bind:@"originX" toObject:newDataObject withKeyPath:@"originX" options:nil];
		[dummy bind:@"originY" toObject:newDataObject withKeyPath:@"originY" options:nil];
		[dummy bind:@"width" toObject:newDataObject withKeyPath:@"width" options:nil];
		[dummy bind:@"height" toObject:newDataObject withKeyPath:@"height" options:nil];
		[dummy bind:@"tag" toObject:newDataObject withKeyPath:@"tag" options:nil];
		[dummy bind:@"verticalOffset" toObject:newDataObject withKeyPath:@"verticalOffset" options:nil];
		
		[dummy bind:@"inputs" toObject:newDataObject withKeyPath:@"inputs" options:nil];
		[dummy bind:@"outputs" toObject:newDataObject withKeyPath:@"outputs" options:nil];
		
		[newDataObject bind:@"originX" toObject:dummy withKeyPath:@"originX" options:nil];
		[newDataObject bind:@"originY" toObject:dummy withKeyPath:@"originY" options:nil];
		[newDataObject bind:@"width" toObject:dummy withKeyPath:@"width" options:nil];
		[newDataObject bind:@"height" toObject:dummy withKeyPath:@"height" options:nil];
		
		[newDataObject bind:@"inputs" toObject:dummy withKeyPath:@"inputs" options:nil];
		[newDataObject bind:@"outputs" toObject:dummy withKeyPath:@"outputs" options:nil];
		
		[dummy setValue:newDataObject forKeyPath:@"data"];
		
		NSArray *keys = [[newDataObject class] keysForNonBoundsProperties];
		NSEnumerator *keyEnumerator = [keys objectEnumerator];
		NSString *key;
		while ((key = [keyEnumerator nextObject]))
		{
			[newDataObject addObserver:self
							forKeyPath:key
							   options:nil
							   context:_propertyObservationContext];
		}
	}
}

- (void)stopObservingDataObjects:(NSArray *)dataObjects
{
	if ([dataObjects isEqual:[NSNull null]])
	{
		return;
	}
	
	NSEnumerator *dataObjectsEnumerator = [dataObjects objectEnumerator];
	
    id oldDataObject;
    while ((oldDataObject = [dataObjectsEnumerator nextObject]))
	{
		[oldDataObject removeObserver:self forKeyPath:@"drawingBounds"];
		NSArray *keys = [[oldDataObject class] keysForNonBoundsProperties];
		NSEnumerator *keyEnumerator = [keys objectEnumerator];
		NSString *key;
		while ((key = [keyEnumerator nextObject]))
		{
			[oldDataObject removeObserver:self forKeyPath:key];
		}
		[oldDataObject unbind:@"originX"];
		[oldDataObject unbind:@"originY"];
		
		EFView *aView;
		NSEnumerator *enu = [[self subviews] objectEnumerator];
		while ((aView = [enu nextObject])) {
			if ([aView valueForKey:@"data"] == oldDataObject) {
				
				[aView unbind:@"title"];
				[aView unbind:@"titleColor"];
				[aView unbind:@"originX"];
				[aView unbind:@"originY"];
				[aView unbind:@"width"];
				[aView unbind:@"height"];
				[aView unbind:@"tag"];
				[aView unbind:@"verticalOffset"];
				
				[aView unbind:@"inputs"];
				[aView unbind:@"outputs"];
				
				[aView removeFromSuperview];
			}
		}
	}
}



- (void)observeValueForKeyPath:(NSString *)keyPath
					  ofObject:(id)object
						change:(NSDictionary *)change
					   context:(void *)context
{
    if (context == _dataObjectsObservationContext)
	{
		
		NSArray *_newDataObjects = [object valueForKeyPath:dataObjectsKeyPath];		
		
		NSMutableArray *onlyNew = [_newDataObjects mutableCopy];
		[onlyNew removeObjectsInArray:oldDataObjects];
		[self startObservingDataObjects:onlyNew];
		[onlyNew release];
		
		NSMutableArray *removed = [oldDataObjects mutableCopy];
		[removed removeObjectsInArray:_newDataObjects];
		[self stopObservingDataObjects:removed];
		[removed release];
		
		[self setOldDataObjects:_newDataObjects];
		[self setNeedsDisplay:YES];
		return;
    }
	if (context == _propertyObservationContext)
	{
		[self setNeedsDisplay:YES];
		return;
	}
	
	if (context == _selectionIndexesObservationContext)
	{
		
		[self setNeedsDisplay:YES];
		return;
	}
}

#pragma mark -
#pragma mark Containers

@synthesize dataObjectsContainer;
@synthesize selectionIndexesContainer;
@synthesize dataObjectsKeyPath;
@synthesize selectionIndexesKeyPath;


#pragma mark -
#pragma mark *** setters and accessors ***

@synthesize delegate;

// dataObjects

- (NSArray *)dataObjects
{
	return [dataObjectsContainer valueForKeyPath:dataObjectsKeyPath];
}


- (EFView*)viewForData:(id)aData
{
	NSEnumerator *enu = [[self subviews] objectEnumerator];
	EFView* aView;
	while (aView = [enu nextObject]) {
		if ([aView valueForKey:@"data"] == aData) {
			return aView;
		}
	}
	return nil;
}

- (NSIndexSet *)selectionIndexes
{
	return [selectionIndexesContainer valueForKeyPath:selectionIndexesKeyPath];
}

- (NSArray *)oldDataObjects { return oldDataObjects; }

- (void)setOldDataObjects:(NSArray *)anOldDataObjects
{
    if (oldDataObjects != anOldDataObjects) {
        [oldDataObjects release];
        oldDataObjects = [anOldDataObjects mutableCopy];
    }
}

- (NSMutableArray *)laces
{
	NSMutableArray* _laces = [[NSMutableArray alloc]init];
	
	NSEnumerator *startObjects = [[self dataObjects] objectEnumerator];
	id startObject;
	while (startObject = [startObjects nextObject])
	{
		id startHoles = [startObject valueForKey:@"outputs"];
		if ([startHoles count]>0)
		{
			NSEnumerator *startHolesEnum = [startHoles objectEnumerator];
			id aStartHole;
			while (aStartHole = [startHolesEnum nextObject])
			{
				NSSet * endHoles = [aStartHole valueForKey:@"laces"];
				if ([endHoles count]>0)
				{
					NSEnumerator * endHolesEnum = [endHoles objectEnumerator];
					id aEndHole;
					while (aEndHole = [endHolesEnum nextObject])
					{
						NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:aStartHole,@"startHole",aEndHole,@"endHole",nil];
						[_laces addObject:aDict];
					}
				}
			}
		}
	}
	
	return _laces;
}


#pragma mark -
#pragma mark *** drawing ***


-(void)drawLinkFrom:(NSPoint)aStartPoint to:(NSPoint)aEndPoint color:(NSColor *)insideColor {
	// a lace is made of an outside gray line of width 5, and a inside insideColor(ed) line of width 3
	
	NSPoint p0 = NSMakePoint(aStartPoint.x, aStartPoint.y);
	NSPoint p3 = NSMakePoint(aEndPoint.x, aEndPoint.y);
	
	NSPoint p1 = NSMakePoint(aStartPoint.x + treshold((aEndPoint.x - aStartPoint.x) / 2, 50), aStartPoint.y);
	NSPoint p2 = NSMakePoint(aEndPoint.x - treshold((aEndPoint.x - aStartPoint.x) / 2, 50), aEndPoint.y);
	
	//p0 and p1 are on the same horizontal line
	//distance between p0 and p1 is set with the treshold fuction
	//the same holds for p2 and p3
	
	NSBezierPath* path = [NSBezierPath bezierPath];
	[path setLineWidth:0];
	[[NSColor grayColor] set];
	[path appendBezierPathWithOvalInRect:NSMakeRect(aStartPoint.x-2.5,aStartPoint.y-2.5,5,5)];
	[path fill];
	
	path = [NSBezierPath bezierPath];
	[path setLineWidth:0];
	[insideColor set];
	[path appendBezierPathWithOvalInRect:NSMakeRect(aStartPoint.x-1.5,aStartPoint.y-1.5,3,3)];
	[path fill];
	
	path = [NSBezierPath bezierPath];
	[path setLineWidth:0];
	[[NSColor grayColor] set];
	[path appendBezierPathWithOvalInRect:NSMakeRect(aEndPoint.x-2.5,aEndPoint.y-2.5,5,5)];
	[path fill];
	
	path = [NSBezierPath bezierPath];
	[path setLineWidth:0];
	[insideColor set];
	[path appendBezierPathWithOvalInRect:NSMakeRect(aEndPoint.x-1.5,aEndPoint.y-1.5,3,3)];
	[path fill];
	
	path = [NSBezierPath bezierPath];
	[path setLineWidth:5];
	[path moveToPoint:p0];
	[path curveToPoint:p3 controlPoint1:p1 controlPoint2:p2];
	[[NSColor grayColor] set];
	[path stroke];
	
	
	path = [NSBezierPath bezierPath];
	[path setLineWidth:3];
	[path moveToPoint:p0];
	[path curveToPoint:p3 controlPoint1:p1 controlPoint2:p2];
	[insideColor set];
	[path stroke];
}

- (BOOL)isOpaque {
	return YES;
}

- (void)drawRect:(NSRect)rect {
	// Draw frame
	NSEraseRect(rect);
	if (![NSGraphicsContext currentContextDrawingToScreen]) {
		NSFrameRect([self bounds]);
	}
	
	// Draw laces
	NSEnumerator *startObjects = [[self dataObjects] objectEnumerator];
	id startObject;
	while (startObject = [startObjects nextObject]) {
		id startHoles = [startObject valueForKey:@"outputs"];
		if ([startHoles count] > 0) {
			EFView *startView = [self viewForData:startObject];
			NSEnumerator *startHolesEnum = [startHoles objectEnumerator];
			id aStartHole;
			while (aStartHole = [startHolesEnum nextObject]) {
				NSSet *endHoles = [aStartHole valueForKey:@"laces"];
				
				if ([endHoles count] > 0) {
					NSPoint aStartPoint = [startView startHolePoint:aStartHole];
					NSEnumerator *endHolesEnum = [endHoles objectEnumerator];
					id aEndHole;
					
					while (aEndHole = [endHolesEnum nextObject]) {
						id endData = [aEndHole valueForKey:@"data"];
						EFView *endView = [self viewForData:endData];
						NSPoint aEndPoint = [endView endHolePoint:aEndHole];
						if (([selectedLace valueForKey:@"startHole"] == aStartHole)
							&&([selectedLace valueForKey:@"endHole"] == aEndHole)) {
							[self drawLinkFrom:aStartPoint
											to:aEndPoint
										 color:([NSGraphicsContext currentContextDrawingToScreen])?[NSColor whiteColor]:[NSColor yellowColor]];
						}
						else {
							[self drawLinkFrom:aStartPoint to:aEndPoint color:[NSColor yellowColor]];
						}
					}
				}
			}
		}
	}
	
	// Draw lace being created
	if (isMaking)
	{
		
		if (([self isEndHole:endPoint])&&(endSubView != startSubView)) {
			endPoint = [endSubView endHolePoint:endHole];
			[self drawLinkFrom:startPoint to:endPoint color:[NSColor yellowColor]];
		}
		else
		{
			[self drawLinkFrom:startPoint
							to:endPoint
						 color:([NSGraphicsContext currentContextDrawingToScreen])?[NSColor whiteColor]:[NSColor yellowColor]];
		}
	}
	
	// Draw selection rubber band
	if (isRubbing) {
		NSRect rubber = NSUnionRect(NSMakeRect(rubberStart.x,rubberStart.y,0.1f,0.1f),NSMakeRect(rubberEnd.x,rubberEnd.y,0.1f,0.1f));
		[NSBezierPath setDefaultLineWidth:0.5f];
		[[[[NSColor whiteColor] blendedColorWithFraction:0.2f ofColor:[NSColor blackColor]]colorWithAlphaComponent:0.3f] setFill];
		[NSBezierPath fillRect:rubber];
		[[NSColor whiteColor] setStroke];
		[NSBezierPath setDefaultLineWidth:1.0f];
		//NSFrameRect(rubber);
		[NSBezierPath strokeRect:rubber];
	}
}	

#pragma mark -
#pragma mark *** geometry ***

- (void)deselectViews
{
	[selectionIndexesContainer setValue:nil forKeyPath:selectionIndexesKeyPath];
	[[self subviews] makeObjectsPerformSelector:@selector(deselect)];
	
}

- (void)selectView:(EFView *)aView
{
	[self selectView:aView state:YES];
}

- (void)selectView:(EFView *)aView state:(BOOL)aBool
{
	NSIndexSet *selection = nil;
	unsigned int DataObjectIndex;
	DataObjectIndex = [[self dataObjects] indexOfObject:[aView valueForKey:@"data"]];
	
	selection = [[[self selectionIndexes] mutableCopy] autorelease];
	if (aBool) {
		[(NSMutableIndexSet *)selection addIndex:DataObjectIndex];
	}
	else
	{
		[(NSMutableIndexSet *)selection removeIndex:DataObjectIndex];
	}
	[selectionIndexesContainer setValue:selection forKeyPath:selectionIndexesKeyPath];
	
	[aView setSelected:aBool];
}

- (BOOL)isStartHole:(NSPoint)aPoint
{
	EFView *aView;
	NSEnumerator *enu =[[self subviews] objectEnumerator];
	while ((aView = [enu nextObject]))
	{
		if ([aView startHole:aPoint] != nil )
		{
			startSubView = aView;
			startHole = [aView startHole:aPoint];
			return YES;
		}
	}
	return NO;
}

- (BOOL)isEndHole:(NSPoint)aPoint
{
	EFView *aView;
	NSEnumerator *enu =[[self subviews] objectEnumerator];
	while ((aView = [enu nextObject]))
	{
		if ([aView endHole:aPoint] != nil )
		{
			endSubView = aView;
			endHole = [aView endHole:aPoint];
			return YES;
		}
	}
	return NO;
}

- (id) laceAtPoint:(NSPoint)aPoint
{
	NSEnumerator *enu = [[self laces] objectEnumerator];
	NSDictionary *aDict;
	while ((aDict = [enu nextObject]))
	{
		NSDictionary* aStartHole = [aDict objectForKey:@"startHole"];
		NSDictionary* aEndHole = [aDict objectForKey:@"endHole"] ;
		
		id startData = [aStartHole valueForKey:@"data"];
		id endData = [aEndHole valueForKey:@"data"];
		
		NSPoint aStartPoint = [[self viewForData:startData] startHolePoint:aStartHole];
		NSPoint aEndPoint = [[self viewForData:endData] endHolePoint:aEndHole];		
		
		NSPoint p0 = NSMakePoint(aStartPoint.x,aStartPoint.y );
		NSPoint p1 = NSMakePoint(aStartPoint.x+treshold((aEndPoint.x - aStartPoint.x)/2,30),aStartPoint.y);
		NSPoint p2 = NSMakePoint(aEndPoint.x -treshold((aEndPoint.x - aStartPoint.x)/2,30),aEndPoint.y );	
		NSPoint p3 = NSMakePoint(aEndPoint.x,aEndPoint.y );
		
		float dx = p0.y - p3.y;
		float dy = p3.x - p0.x;
		float d = sqrt(dx*dx+dy*dy);
		dx = 5*dx/d;
		dy = 5*dy/d;
		
		NSBezierPath *path = [NSBezierPath bezierPath];
		[path moveToPoint:NSMakePoint(p0.x-dx, p0.y-dy)];
		[path curveToPoint:NSMakePoint(p3.x-dx, p3.y-dy) controlPoint1:NSMakePoint(p1.x-dx, p1.y-dy) controlPoint2:NSMakePoint(p2.x-dx, p2.y-dy)];
		[path lineToPoint:NSMakePoint(p3.x+dx, p3.y+dy)];
		[path curveToPoint:NSMakePoint(p0.x+dx, p0.y+dy) controlPoint1:NSMakePoint(p2.x+dx, p2.y+dy) controlPoint2:NSMakePoint(p1.x+dx, p1.y+dy)];
		[path closePath];
		
		if([path containsPoint:aPoint])
		{
			return aDict;
		}
	}
	return nil;
}

#pragma mark -
#pragma mark *** connections ***

- (void) connectHole:(id)aStartHole toHole:(id)aEndHole {
	
	if ([aStartHole valueForKey:@"data"] == [aEndHole valueForKey:@"data"]) {
		return;
	}
	
	NSDictionary *conn = [NSDictionary dictionaryWithObjectsAndKeys:
		aStartHole,@"startHole",
		aEndHole,@"endHole",nil];
	
	// check if already connected
	NSEnumerator *enu = [[self laces] objectEnumerator];
	NSDictionary *aDict;
	while ((aDict = [enu nextObject])) {
		if ([conn isEqualToDictionary:aDict])
		{
			return;
		}
	}
	[self willChangeValueForKey:@"laces"];
	[[self laces] addObject: conn];
	[self didChangeValueForKey:@"laces"];
	
	[[aStartHole mutableSetValueForKey:@"laces"] addObject:aEndHole];
}

#pragma mark -
#pragma mark *** events ***

- (BOOL) acceptsFirstResponder {
	return YES;
}

- (void)keyDown:(NSEvent*)theEvent {
	if(([theEvent keyCode] == 51) && selectedLace) {
		id aStartHole = [selectedLace valueForKey:@"startHole"];
		id aEndHole = [selectedLace valueForKey:@"endHole"];
		
		[self willChangeValueForKey:@"laces"];
		[aEndHole willChangeValueForKey:@"laces"];
		[aStartHole willChangeValueForKey:@"laces"];
		
		[[aStartHole mutableSetValueForKey:@"laces"] removeObject:aEndHole];
		[[aEndHole mutableSetValueForKey:@"laces"] removeObject:aStartHole];
		
		[aEndHole didChangeValueForKey:@"laces"];
		[aStartHole didChangeValueForKey:@"laces"];
		[self didChangeValueForKey:@"laces"];
		
		selectedLace = nil;
		[self setNeedsDisplay:YES];
		return;
	}
	
	if (([theEvent keyCode] == 53) && ([[self selectionIndexes] count]>0)) {
		[self deselectViews];
		[self setNeedsDisplay:YES];
		return;
	}
	
	NSBeep();
}

-(void)mouseDown:(NSEvent*)theEvent
{
	NSPoint mouseLoc = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	
	// Did we click on a start hole ?
	if (![self isStartHole:mouseLoc]) // clicked outside a hole for begining a new lace
	{
		// Did we click on an end hole ?
		if (![self isEndHole:mouseLoc]) // clicked outside any hole : so manage selections
		{
			id select = [self laceAtPoint:mouseLoc ];
			if (!select) // didn't click on a lace so select/deselect views
			{
				[self deselectViews];
				
				//Rubberband selection
				isRubbing = YES;
				rubberStart = mouseLoc;
				rubberEnd = mouseLoc;
				BOOL keepOn = YES;
				
				NSRect rubber;
				
				while (keepOn)
				{
					theEvent = [[self window] nextEventMatchingMask: NSLeftMouseUpMask |
						NSLeftMouseDraggedMask];
					mouseLoc = [self convertPoint:[theEvent locationInWindow] fromView:nil];
					rubberEnd = mouseLoc;
					rubber = NSUnionRect(NSMakeRect(rubberStart.x,rubberStart.y,0.1,0.1),NSMakeRect(rubberEnd.x,rubberEnd.y,0.1,0.1));
					
					switch ([theEvent type])
					{
						case NSLeftMouseDragged:
						{
							// find views partially inside rubber and select them

							NSEnumerator *enu = [[self subviews] objectEnumerator];
							EFView* aView;
							while (aView = [enu nextObject])
							{
								[self selectView:aView state:NSIntersectsRect([aView frame],rubber)];
							}
							[self setNeedsDisplay:YES];
							break;
						}
						case NSLeftMouseUp:
						{
							keepOn = NO;
							isRubbing = NO;
														
							[self setNeedsDisplay:YES];
							break;
						}
						default:
							/* Ignore any other kind of event. */
							break;
					}
				}
			}
			
			// We clicked on a lace
			if (select != selectedLace) // change selection of lace
			{
				selectedLace = select;
				[self setNeedsDisplay:YES];
			}
			return;
		}
		
		// We clicked on an end hole
		
		/* Dragging from an existing connection end will disconnect and recontinue the drag */
		NSEnumerator *enu = [[self laces] reverseObjectEnumerator]; // last created lace first 
		NSDictionary *aDict;
		while ((aDict = [enu nextObject]))
		{
			if([aDict objectForKey:@"endHole"] == endHole) break;
		}
		if(!aDict) return; //nothing to un-drag...
		
		startHole = [aDict objectForKey:@"startHole"];
		startSubView = [self viewForData:[startHole valueForKey:@"data"]];
		
		[startHole willChangeValueForKey:@"laces"];
		[endHole willChangeValueForKey:@"laces"];
		[self willChangeValueForKey:@"laces"];
		
		[[startHole mutableSetValueForKey:@"laces"] removeObject:endHole];
		[[endHole mutableSetValueForKey:@"laces"] removeObject:startHole];
		
		selectedLace = nil; //deselect previously selected lace
		
		startPoint = [startSubView startHolePoint:startHole];
	}
	
	else // we clicked on a start hole
	{
		startPoint = [startSubView startHolePoint:startHole];
	}
	
	isMaking= YES;
	BOOL keepOn = YES;
	BOOL isInside = YES;
	
	[[NSCursor crosshairCursor] set];
	while (keepOn) {
		theEvent = [[self window] nextEventMatchingMask: NSLeftMouseUpMask |
			NSLeftMouseDraggedMask];
		mouseLoc = [self convertPoint:[theEvent locationInWindow] fromView:nil];
		isInside = [self mouse:mouseLoc inRect:[self bounds]];
		switch ([theEvent type]) {
			case NSLeftMouseDragged:
				endPoint = mouseLoc;
				[self setNeedsDisplay:YES];
				break;
			case NSLeftMouseUp:
				[[NSCursor arrowCursor] set];
				if ([self isEndHole:mouseLoc]) {
					[self connectHole:startHole toHole:endHole];
				}
					keepOn = NO;
				isMaking = NO;
				[startHole didChangeValueForKey:@"laces"];
				[endHole didChangeValueForKey:@"laces"];
				[self didChangeValueForKey:@"laces"];
				
				[self setNeedsDisplay:YES];
				break;
			default:
				/* Ignore any other kind of event. */
				break;
		}
	};
	return;
}

@end
